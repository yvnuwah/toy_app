# Toy Micropost App

This is the second application for the
[*Ruby on Rails Tutorial*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## Summary
This chapter gives a very high-level overview of Rails through the creation of a Twitter-esque posting app.

## Links
As of 07/19/2015, the app can be publicly viewed [HERE](https://serene-stream-1973.herokuapp.com) to view and create users and [HERE](https://serene-stream-1973.herokuapp.com/microposts) to view and create microposts.